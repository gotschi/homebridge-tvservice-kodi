var Service, Characteristic, VolumeCharacteristic;

const request = require('request');

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  
  homebridge.registerAccessory("homebridge-tvservice-kodi", "tvservice-kodi", KodiAccessory);
}

function KodiAccessory(log, config) {
  this.log = log;
  this.name = config["name"];
  this.host = config["host"];
  this.port = config["port"];
  
  var that = this;

  this.services = [];
  this.service = new Service.Television();

  this.service.setCharacteristic(Characteristic.ConfiguredName, this.name);
  this.services.push(this.service);
  
  this.inputService = new Service.InputSource(this.name);
  
  this.speakerService = new Service.TelevisionSpeaker(
      this.name + " Volume",
      "volumeService"
    );

    this.speakerService
      .setCharacteristic(Characteristic.Active, Characteristic.Active.ACTIVE)
      .setCharacteristic(
        Characteristic.VolumeControlType,
        Characteristic.VolumeControlType.ABSOLUTE
      );

    this.speakerService
      .getCharacteristic(Characteristic.VolumeSelector)
      .on("set", function(code, callback) {
          
          var json;
          
          json = { "method": "Application.SetVolume" };
          
      if(code === 0) {
          json["params"] = { "volume": "increment" };
      }
      else {
          json["params"] = { "volume": "decrement" };
      }
    
    json["jsonrpc"] = "2.0";
    json["id"] = 1;

    request({
        method: 'POST',
        uri: 'http://'+that.host+":"+that.port+'/jsonrpc',
        json: json
    },
    function (error, response, body) {
        if (error) {
            callback(false);
            that.log.debug('upload failed:', error);
        }
        else callback(null);
    });
          
      });
  
  this.services.push(this.speakerService);

  this.service.getCharacteristic(Characteristic.Active)
  .on('set', function(newValue, callback) {
    callback(null);
  })
  .on('get', function(callback) {
    callback(null, 1);
  });

  this.service.getCharacteristic(Characteristic.RemoteKey)
  .on('set', function(newValue, callback) {

      var json;
      
    switch (newValue) {
      case 4:
          json={"method": "Input.Up"};
          break;
      case 5:
          json={"method": "Input.Down"};
          break;
      case 6:
          json={"method": "Input.Left"};
          break;
      case 7:
          json={"method": "Input.Right"};
          break;
      case 8:
          json={"method": "Input.Select"};
          break;
      case 9:
          json={"method": "Input.Back"};
          break;
      case 15:
          json={"method": "Input.Info"};
          break;
      case 10:
          json={"method": "Player.Stop", "params": { "playerid": 1 }};
          break;
      case 11:
          json={"method": "Player.PlayPause", "params": { "playerid": 1 }};
          break;
    }
    
    json["jsonrpc"] = "2.0";
    json["id"] = 1;

    request({
        method: 'POST',
        uri: 'http://'+that.host+":"+that.port+'/jsonrpc',
        json: json
    },
    function (error, response, body) {
        if (error) {
            return console.error('upload failed:', error);
        }
    });

    callback('null');
  });
  
}

KodiAccessory.prototype.getServices = function() {
  return this.services;
}
